TARGET=neutrino

fpga_$(TARGET).bin: bit.bif $(TARGET)_top.bit
	bootgen -image bit.bif -arch zynqmp -process_bitstream bin
	mv $(TARGET)_top.bit.bin fpga_$(TARGET).bin

bit.bif:
	@echo "all:\n{\n $(TARGET)_top.bit\n}" > bit.bif

$(TARGET)_top.bit: $(TARGET)_top.hdf
	unzip -o $(TARGET)_top.hdf $(TARGET)_top.bit
	touch -c $(TARGET)_top.bit # Update timestamp to now to prevent unzip again if try to remake

clean:
	rm -f bit.bif *_top.bit *.bin
